#-------------------------------------------------
#
# Project created by QtCreator 2015-10-04T19:57:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MakeMeMove
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    Gui/editor.cpp \
    Gui/mainwidget.cpp \
    Gui/logwidget.cpp \ 
    debug/moc_mainwindow.cpp \
    Constructions/frame.cpp \
    Constructions/concept.cpp \
    Constructions/individ.cpp

HEADERS  += mainwindow.h \
    Gui/editor.h \
    Gui/mainwidget.h \
    Gui/logwidget.h \   
    Constructions/frame.h \
    Constructions/concept.h \
    Constructions/individ.h

FORMS    +=
