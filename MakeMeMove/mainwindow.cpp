#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <Gui/mainwidget.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{

    MainWidget *main = new MainWidget(this);
    setCentralWidget(main);
    this->show();
}

MainWindow::~MainWindow()
{

}
