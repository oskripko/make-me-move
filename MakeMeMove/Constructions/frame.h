#ifndef FRAME_H
#define FRAME_H

#include <QtCore/QString>
#include <QtCore/QList>
#include <QtCore/QPair>

#include "individ.h"
#include "concept.h"


class Frame
{


    enum FrameType
    {
        Event,
        Character,
        Functional
    };

    enum FrameRole
    {
    //EventFrameRoles
        agent,
        object,
        source,
        destination,
        EventResult,

    //CharFrameRoles
        charachter,
        value,

    //FuncFrameRoles
        argument,
        FuncResult
    };

	//typedef QList< QPair<FrameRole, Individ>> Slot; 


public:
    Frame(FrameType type, QString name);
    void addSlot(FrameRole role, QString name, Concept type);
	//Individ individs;
private:
    FrameType f_type;
    QString f_name;
    QList< QPair<FrameRole, Individ> > f_slots;

};

#endif // FRAME_H
