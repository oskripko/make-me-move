#ifndef INDIVID_H
#define INDIVID_H

#include "concept.h"

#include <QtCore/QString>

class Individ
{
	public:
		Individ(QString name, Concept type);
		Individ();
	private:
		QString i_name;
		Concept i_type;
};

#endif // INDIVID_H
