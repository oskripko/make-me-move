#include "addInitial.h"

#include <QtWidgets/QTabWidget>
#include <QtWidgets/QHBoxLayout>

#include <Gui/fabric.h>
#include <Gui/properties.h>


AddInitial::AddInitial()
{
   QHBoxLayout *mainLayout = new QHBoxLayout;

   QTabWidget *tabWidget = new QTabWidget();

   tabWidget->addTab(new Fabric(Tab::CONCEPT), tr("Концепты"));
   tabWidget->addTab(new Fabric(Tab::CONCEPT), tr("Переменные"));
   tabWidget->addTab(new Fabric(Tab::CONCEPT), tr("Константы"));
   tabWidget->addTab(new Fabric(Tab::CONCEPT), tr("Фреймы"));
   tabWidget->addTab(new Fabric(Tab::CONCEPT), tr("Виды фреймов"));

   tabWidget->setTabPosition(QTabWidget::West);

   tabWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
   mainLayout->addWidget(tabWidget);

   Properties *prop = new Properties(this);
   mainLayout->addWidget(prop);

   setLayout(mainLayout);
}

