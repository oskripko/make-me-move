#include "bonds.h"

#include <QtWidgets/QTextEdit>
#include <QtWidgets/QHBoxLayout>


Bonds::Bonds()
{
    QTextEdit *editor = new QTextEdit(this);

    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    editor->setSizePolicy(sizePolicy);
    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->addWidget(editor);
}

