#include "properties.h"

#include <QtWidgets/QTextEdit>
#include <QtWidgets/QHBoxLayout>


Properties::Properties(QWidget *parent) :
    QWidget(parent)
{
    QTextEdit *editor = new QTextEdit(this);

    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    editor->setSizePolicy(sizePolicy);
    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->addWidget(editor);
}

