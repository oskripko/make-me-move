#include "mainwidget.h"

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QTabWidget>

#include <Gui/bonds.h> //Поменять систему инклюдов
#include <Gui/addInitial.h>

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent)
{
    QTabWidget *tabWidget = new QTabWidget();
    tabWidget->addTab(new AddInitial(), tr("Данные"));
    tabWidget->addTab(new Bonds(), tr("Связи"));

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(tabWidget);
    setLayout(mainLayout);
}

