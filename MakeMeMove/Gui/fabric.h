#ifndef ADDTYPE_H
#define ADDTYPE_H

#include "Workout/dbwout.h"

#include <QWidget>

enum Tab
{
    CONCEPT,
    VARIABLE,
    CONSTANT,
    FRAME,
    FRAMETYPE

};

class Fabric : public QWidget
{
    Q_OBJECT
public:

    Fabric(Tab tapType);
private:
    runAdding();

    DbWOut wo;
    Tab tabType;

public slots:
    bool addRecord();
};

#endif // ADDTYPE_H
