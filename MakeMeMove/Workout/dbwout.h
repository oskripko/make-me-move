#ifndef DBWOUT_H
#define DBWOUT_H

#include "dbconf.h"

#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QVariant>

class DbWOut
{
public:
    DbWOut();
    ~DbWOut();

    bool insertPrimeConcept(QString name);
    QSqlRecord getConcept();
private:

    DbConf *conf;
    QSqlQuery query;

};

#endif // DBWOUT_H
