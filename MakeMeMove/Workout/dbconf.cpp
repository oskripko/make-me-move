#include "dbconf.h"

#include <iostream>

DbConf::DbConf():db(QSqlDatabase()){
    db = QSqlDatabase::addDatabase("QPSQL", "connection");
    db.setHostName("localhost");
    db.setDatabaseName("mmm");
    db.setUserName("postgres");
    db.setPassword("123456Ww");
    if(db.open())
        query = QSqlQuery(db);
    //std::cout << ok << std::flush;
    createTables();
}

DbConf::~DbConf(){
    db.close();
    QSqlDatabase::removeDatabase("connection");
}

bool DbConf::createTables(){
    if(!query.exec("CREATE TABLE IF NOT EXISTS Concept ("
                   "id serial CONSTRAINT firstkey PRIMARY KEY,"
                   "name varchar(40) NOT NULL,"
                   "defun text,"
                   "primal boolean) "))
        std::cout << "Concept\n" << std::flush;
    else if(!query.exec("CREATE TABLE IF NOT EXISTS Frame ("
                        "id serial PRIMARY KEY,"
                        "name varchar(40) NOT NULL,"
                        "type varchar(50) NOT NULL) "))
        std::cout << "Frame\n";
    else if(!query.exec("CREATE TABLE IF NOT EXISTS Individ ("
                        "id serial PRIMARY KEY,"
                        "name varchar(40) NOT NULL,"
                        "type varchar(50) NOT NULL,"
                        "isvariable boolean)"))
        std::cout << "Individ\n";
//    else if(!query.exec("CREATE TABLE IF NOT EXISTS Consts ()"))
//        std::cout << "Consts\n";
    else if(!query.exec("CREATE TABLE IF NOT EXISTS FrameTypes ("
                        "id serial PRIMARY KEY,"
                        "name varchar(40) NOT NULL,"
                        "roles varchar(40) ARRAY)"))
        std::cout << "FrameTypes\n";
    return true;
}
