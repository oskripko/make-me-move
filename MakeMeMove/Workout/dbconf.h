#ifndef DBCONF_H
#define DBCONF_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>


class DbConf
{
public:
    DbConf();
    ~DbConf();

    bool createTables();

    QSqlDatabase db;
    QSqlQuery query;

private:

};

#endif // DBCONF_H
